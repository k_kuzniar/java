package pk.java.projekt.krzyzowka;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import pk.java.projekt.tablica.Board;
import pk.java.projekt.dictionary.CwEntry;
import pk.java.projekt.dictionary.Entry;
import pk.java.projekt.dictionary.InteliCwDB;


/**
 * Klasa reprezetujaca cala krzyzowke
 *
 *
 */
public class Crossword implements Serializable{

	 private LinkedList<CwEntry> entries=new LinkedList<CwEntry>();
	 private Board b;
	 private InteliCwDB cwdb;
	 private final long ID;// domyslnie -1
	 private Entry haslo;
	/**
	 * Konstruktor z wielkoscia board
	 * @param height
	 * @param width
	 */
	 public Crossword(int height, int width){
		 Calendar cal = Calendar.getInstance();
		 ID=cal.getTimeInMillis();
		 b=new Board(height, width);
	 }
	 /**
	  * Konstruktor z parametrem ID i wielkoscia board
	  * @param ID
	  * @param height
	  * @param width
	  */
	 public Crossword(long ID, int height, int width){
		 this.ID=ID;
		 b=new Board(height, width);
	 }
	 /**
	  * Metoda zwracajaca iterator Read Only
	  * @return Iterator<CwEntry>
	  */
	 public Iterator<CwEntry> getROEntryIter(){
		 if(entries==null) return null;
		 else return Collections.unmodifiableList(entries).iterator();
	 }
	 /**
	  * Kopiuje tablice
	  * @return Board
	  * @see tablica.Board Board
	  */
	 public Board getBoardCopy(){
		 return new Board(b);
	 }
	 /**
	  * Metoda zwracajaca cwdb
	  * @return InteliCwDB
	  */
	 public InteliCwDB getCwDB(){
		 return cwdb;
	 }
	 /**
	  * Metoda ustawiajaca cwdb
	  * @param cwdb InteliCwDB
	  */
	 public void setCwDB(InteliCwDB cwdb){
		 this.cwdb=cwdb;
	 }
	 /**
	  * Metoda sprawdzajaca czy lista slow zawiera podane slowo
	  * @param word String
	  * @return boolean
	  */
	 public boolean contains(String word){
		 if(cwdb.get(word).equals(null)) return false;
		 else return true;
	 }
	 /**
	  * Metoda dodajaca nowe entry i wymuszajaca aktualizacje tablicy
	  * @param cwe CwEntry
	  * @param s Strategy
	  */
	 public final void addCwEntry(CwEntry cwe, Strategy s){
		 entries.add(cwe);
		 s.updateBoard(b,cwe);
	 }
	 /**
	  * Metoda wywolujaca metode ktora sprawdzi czy wszystkie slowa sa dodane do listy
	  * @param s Strategy
	  */
	 public final void generate(Strategy s){
		 Entry e = null;
		 while((e = s.findEntry(this)) != null){
		    addCwEntry((CwEntry)e,s);
		 }
	 }
	 /**
	  * Metoda zwracajaca ID
	  * @return long
	  */
	 public long getID(){
		 return ID;
	 }
	 /**
	  * Metoda do wypisywania krzyzowki w trybie tekstowym
	  * Uzywane do testow w trybie tekstowym
	  */
	 public void out(){
		 for(int i=0; i<entries.size(); i++){
			 System.out.println(entries.get(i).getWord());
		 }
	 }
	 /**
	  * Metoda zwracajaca wysokosc
	  * @return int
	  */
	 public int getHeight(){
		 return b.getHeight();
	 }
	 /**
	  * Metoda zwracajaca haslo
	  * @return CwEntry
	  */
	 public Entry getHaslo(){
		 return haslo;
	 }
	 /**
	  * Metoda ustawiajaca haslo
	  * @param w CwEntry
	  */
	 public void setHaslo(Entry w){
		 haslo=w;
	 }
}

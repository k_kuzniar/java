package pk.java.projekt.krzyzowka;

import java.io.IOException;

import pk.java.projekt.Exeption.NoSuchCross;

/**
 * Interfejs zapisujacy krzyzowke do pliku
 *
 * @see Generator Generator
 * @see Reader Reader
 */
public interface Writer {
	
	/**
	 * Metoda zapisujaca krzyzowke do pliku
	 * @param w Crossword
	 * @throws NoSuchCross 
	 * @throws IOException 
	 */
	public void write(Crossword w) throws NoSuchCross, IOException;
	/**
	 * Metoda zwracajaca unikalne ID zapisywanej krzyzowki
	 * @return long
	 */
	public long getUniqueID(Crossword w);

}

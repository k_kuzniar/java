package pk.java.projekt.krzyzowka;

import java.io.FileNotFoundException;
import java.io.IOException;

import pk.java.projekt.Exeption.NoSuchCross;

/**
 * Interfejs wczytujacy wszystkie krzyzowki do wybranej struktury danych
 *
 * @see Generator Generator
 * @see Writer Writer
 */
public interface Reader {

	/**
	 * Metoda wczytujaca wszystkie krzyzowki do wybranej struktury danych
	 * @throws FileNotFoundException 
	 * @throws NoSuchCross 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public void getAllCws() throws FileNotFoundException, NoSuchCross, IOException, ClassNotFoundException;
}

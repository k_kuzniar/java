package pk.java.projekt.krzyzowka;

import pk.java.projekt.dictionary.CwEntry;
import pk.java.projekt.tablica.Board;

/**
 * Klasa abstrakcyjna, wykorzystywana do generowania krzyzowek
 *
 * @see Generator Generator
 */
public abstract class Strategy {

	/**
	 * Metoda wyszukujaca czy dana krzyzowka posiada wszystkie hasla
	 * @param cw Crossword
	 * @return nie znalezione haslo
	 */
    public abstract CwEntry findEntry(Crossword cw);
    /**
     * Metoda aktualizujaca tablice
     * @param b Board
     * @param e CwEntry
     */
    public abstract void updateBoard(Board b, CwEntry e);
}

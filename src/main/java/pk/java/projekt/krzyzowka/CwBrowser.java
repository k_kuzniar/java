package pk.java.projekt.krzyzowka;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.LinkedList;

import pk.java.projekt.Exeption.NoSuchCross;
import pk.java.projekt.Exeption.mainFileNotFound;
import pk.java.projekt.Exeption.noWordException;

/**
 * Klasa umozliwiajaca wczytywanie/zapisywanie/przegladanie i generowanie nowych krzyzowek
 *
 *
 */
public class CwBrowser implements Reader, Writer{
	private String adres;
	private Crossword cross=null;
	private LinkedList<Crossword> list=new LinkedList<Crossword>();
	
	/**
	 * Konstruktor
	 * @param filename String
	 */
	public CwBrowser(String filename){
		adres=filename;
	}
	/**
	 * @inheritDoc
	 */
	@Override
	public void write(Crossword w) throws NoSuchCross, IOException {
		if(w==null) throw new NoSuchCross();
		FileOutputStream fos = null;
	    ObjectOutputStream out = null;
		fos = new FileOutputStream(adres + "\\" + w.getID());
	    out = new ObjectOutputStream(fos);
	    out.writeObject(w);
	    out.close();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public long getUniqueID(Crossword w) {
		return w.getID();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void getAllCws() throws NoSuchCross, IOException, ClassNotFoundException {
		File folder=new File(adres);
		File[] pliki=folder.listFiles();
		FileInputStream fis = null;
	    ObjectInputStream in = null;
		for(int i=0; i<pliki.length; i++){
			if(!pliki[i].getName().matches("^[0-9]*$")) continue;
			if(match(Long.valueOf(pliki[i].getName()))) continue;
			
			fis = new FileInputStream(pliki[i]);
		    in = new ObjectInputStream(fis);
		    Crossword tmp=(Crossword) in.readObject();
		    list.add(tmp);
		}
		if(list.size()==0) throw new NoSuchCross();
	}
	/**
	 * Metoda generujaca nowa losowa krzyzowke z ID=-1
	 * @return Crossword
	 * @throws FileNotFoundException
	 * @throws noWordException 
	 * @throws mainFileNotFound 
	 */
	public Crossword generuj(int height, int width) throws FileNotFoundException, noWordException, mainFileNotFound{
		Generator gen=new Generator();
		Crossword c=gen.nowa_p(height, width);
		cross=c;
		return c;
	}
	/**
	 * Zapisuje krzyzowke
	 * @param nr int
	 * @throws NoSuchCross 
	 * @throws IOException 
	 */
	public void save(int nr) throws NoSuchCross, IOException{
		if(cross==null) throw new NoSuchCross();
		write(cross);
	}
	/**
	 * Wypisuje krzyzowke
	 * @throws NoSuchCross 
	 */
	public void out() throws NoSuchCross{
		if(cross==null) throw new NoSuchCross();
		cross.out();
	}
	/**
	 * Metoda zwracajaca krzyzowke
	 * @return Crossword
	 */
	public Crossword getCross(){
		return cross;
	}
	/**
	 * Metoda zmieniajaca wypelnienie
	 * @param path String
	 */
	public void changePath(String path){
		adres=path;
	}
	/**
	 * Metoda sprawdzajaca czy krzyzowka o danym ID jest juz na liscie
	 * @param ID long
	 * @return boolean
	 */
	private boolean match(long ID){
		for(int i=0; i<list.size(); i++){
			if(list.get(i).getID()==ID) return true;
		}
		return false;
	}
	/**
	 * Metoda ustawiajaca krzyzowke
	 * @param i int
	 */
	public void setCross(int i){
		cross=list.get(i);
	}
	/**
	 * Metoda zwracajaca dligosc listy
	 * @return int
	 */
	public int getNumber(){
		return list.size();
	}
	/**
	 * Metoda zwracjaca iterator do list krzyzowek
	 * @return Iterator<Crossword>
	 */
	public Iterator<Crossword> getIterator(){
		return list.iterator();
	}
}

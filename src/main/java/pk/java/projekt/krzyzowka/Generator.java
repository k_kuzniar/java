package pk.java.projekt.krzyzowka;

import java.io.FileNotFoundException;
import java.util.Iterator;

import pk.java.projekt.tablica.Board;
import pk.java.projekt.tablica.BoardCell;
import pk.java.projekt.Exeption.mainFileNotFound;
import pk.java.projekt.Exeption.noWordException;
import pk.java.projekt.dictionary.CwEntry;
import pk.java.projekt.dictionary.Entry;
import pk.java.projekt.dictionary.InteliCwDB;

/**
 * Strategia generowania krzyzowki
 *
 * @see Writer Writer
 * @see Reader Reader
 */
public class Generator extends Strategy {

	private int iteracja=0;
	private static String mainfile=new String("cwdbvw.txt"); //mozliwosc zmiany domyslnego pliku glownego
	private static InteliCwDB cwdb=null;
	
	/**
	 * @inheritDoc
	 */
	@Override
	public CwEntry findEntry(Crossword cw) {
		Iterator<CwEntry> it;
		InteliCwDB cwdb=cw.getCwDB();
		if(iteracja==0){
			iteracja++;
			return new CwEntry(cwdb.getDict(0).getWord(), cwdb.getDict(0).getClue());
		}
		for(; iteracja<cwdb.getSize(); iteracja++){
			boolean nowy=true;
			it=cw.getROEntryIter();
			while(it.hasNext()){ 
				CwEntry tmp=(CwEntry) it.next();
				if(cwdb.getDict(iteracja).getWord().equals(tmp.getWord())) nowy=false;
			}
			if(nowy) return new CwEntry(cwdb.getDict(iteracja).getWord(), cwdb.getDict(iteracja).getClue());
		}
		return null;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void updateBoard(Board b, CwEntry e) {//tylko dla prostych
		int i, j;
		i=e.getX();
		j=e.getY();
		if(b.getCell(0, 0)==null){
			for(;i<e.getWord().length(); i++){
				BoardCell tmp;
				if(i-e.getWord().length()==e.getX()-e.getWord().length()) tmp=new BoardCell(String.valueOf(e.getWord().charAt(i-e.getX())), false, false, BoardCell.Dane.Poz.Start);
				else if (i==e.getX()+e.getWord().length()-1) tmp=new BoardCell(String.valueOf(e.getWord().charAt(i-e.getX())), false, false, BoardCell.Dane.Poz.End);
				else tmp=new BoardCell(String.valueOf(e.getWord().charAt(i-e.getX())), false, false, BoardCell.Dane.Poz.Inner);
				b.setCell(i, j, tmp);
			}
		}
		else{	
			for(j++;j<e.getWord().length(); j++){
				BoardCell tmp;
				if(j-e.getWord().length()==0) tmp=new BoardCell(String.valueOf(e.getWord().charAt(j-e.getX())), false, false, BoardCell.Dane.Poz.Inner);
				else if (j-e.getWord().length()==e.getWord().length()) tmp=new BoardCell(String.valueOf(e.getWord().charAt(j-e.getX())), false, false, BoardCell.Dane.Poz.End);
				else tmp=new BoardCell(String.valueOf(e.getWord().charAt(j-e.getX())), false, false, BoardCell.Dane.Poz.Inner);
				b.setCell(i, j, tmp);
			}
		}
	}
	/**
	 * Generuje nowa losowa krzyzowke
	 * @return Crossword
	 * @throws FileNotFoundException 
	 * @throws noWordException 
	 * @throws mainFileNotFound 
	 */
	public Crossword nowa_p(int height, int width) throws FileNotFoundException, noWordException, mainFileNotFound{
		try{
			if(cwdb==null){
				cwdb=new InteliCwDB(mainfile); //tu mozna zmienic domyslny plik ze wszystkimi slowami
			}
		}
		catch(FileNotFoundException e){
			throw new mainFileNotFound();
		}
		Crossword cross=new Crossword(height, width);
		
		cross.setHaslo(cwdb.getRandom(height)); //Tu mozna zmienic dlugosc generowanej krzyzowki
		InteliCwDB ncwdb=new InteliCwDB();
		
		for(int i=0; i<cross.getHaslo().getWord().length(); i++){
			String regexp=createPattern(String.valueOf(cross.getHaslo().getWord().charAt(i)), width); //create pattern do trybu prostego only
			Entry tmp;
			for(int j=0; true; j++){
				tmp=Generator.cwdb.getRandom(regexp);
				if(ncwdb.getSize()==0) break;
				else if(ncwdb.contains(tmp) || tmp.equals(cross.getHaslo())) continue;//wazne sprawdza, czy slowo jest juz w krzyzowce
				else if(j==30) throw new noWordException();
				else break;
			}
			ncwdb.add(tmp);
		}
		
		cross.setCwDB(ncwdb);
		cross.generate(this);
		
		return cross;
	}
	/**
	 * Metoda zmieniajaca sciezke do pliku glownego
	 * @param path String
	 */
	public static void setMainFile(String path){
		mainfile=path;
	}
	/**
	 * Metoda tworzaca wzor wyrazenia regularnego do trybu prostego
	 * @param litera String
	 * @param width int
	 * @return String
	 */
	private String createPattern(String litera, int width){
		String regexp=new String("^");
    	regexp+=litera;
    	regexp+=".{1," + (width-1) + "}";
    	return regexp;	
	}
}

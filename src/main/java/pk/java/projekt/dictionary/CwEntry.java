package pk.java.projekt.dictionary;

import java.io.Serializable;

/**
 * Rozwiniecie klasy przechowujacej haslo i podpowiedz
 *
 * @see Entry Entry
 */
public class CwEntry extends Entry implements Serializable{

	/**
	 * Enumerate do okreslenia pozycji (pion, poziom)
	 *
	 */
	public enum Direction {
		HORIZ, VERT
		}
	private int x;
	private int y;
	private Direction d;
	
	/**
	 * Konstruktor przyjmujacy slowo i pytanie
	 * @param w String
	 * @param c String
	 * @see Entry#Entry(String, String) Entry(String, String)
	 */
	public CwEntry(String w, String c) {
		super(w, c);
	}
	/**
	 * Konstruktor przyjmujacy wszystkie wartosci
	 * @param w String
	 * @param c String
	 * @param x int
	 * @param y int
	 * @param d Direction
	 */
	public CwEntry(String w, String c, int x, int y, Direction d) {
		super(w, c);
		this.x=x;
		this.y=y;
		this.d=d;
	}
	/**
	 * Zwraca pozycje x
	 * @return int
	 */
	 public int getX(){
		 return x;
	 }
	 /**
	  * Zwraca pozycje y
	  * @return int
	  */
	 public int getY(){
		 return y;
	 }
	 /**
	  * Zwraca polozenie
	  * @return Direction
	  */
	 public Direction getDir(){
		 return d;
	 }
}

package pk.java.projekt.dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Prototyp klasy zawierajacej liste slow
 *
 * @see InteliCwDB InteliCwDB
 */
public class CwDB implements Serializable{
	
	protected LinkedList<Entry> dict;
	
	/**
	 * Konstruktor przyjmujacy sciezke do pliku
	 * @param filename String
	 * @throws FileNotFoundException 
	 * @see dictionary.InteliCwDB#InteliCwDB(String) InteliCwDB(String)
	 * @see #createDB(String) createDB(String)
	 */
	public CwDB(String filename) throws FileNotFoundException{
		dict=new LinkedList<Entry>();
		createDB(filename);
	}
	/**
	 * Konstruktor bezparametrowy
	 * @see InteliCwDB#InteliCwDB() InteliCwDB()
	 */
	public CwDB(){
		dict=new LinkedList<Entry>();
	}
	/**
	 * Dodaje nowe slowo i podpowiedz do listy
	 * @param word String
	 * @param clue String
	 * @see dictionary.InteliCwDB#add(String, String) InteliCwDB.add(String, String)
	 */
	public void add(String word, String clue){
		dict.add(new Entry(word, clue));
	}
	/**
	 * Metoda zwracajaca wskaznik na kontener slowa i podpowiedzi
	 * @param word Slowo
	 * @return Entry
	 */
	public Entry get(String word){
		int i;
		for(i=0; i<dict.size(); i++){
			if(word.equals(dict.get(i).getWord())) break;
		}
		if(i==dict.size()) return null;
		else return dict.get(i);
	}
	/**
	 * Metoda usuwajaca dany wpis z listy
	 * @param word Slowo
	 */
	public void remove(String word){
		int i;
		for(i=0; i<dict.size(); i++){
			if(word.compareToIgnoreCase(dict.get(i).getWord())==0) break;
		}
		if(i==dict.size()) return;
		else dict.remove(i);
	}
	/**
	* Metoda zapisujaca liste slow i podpowiedzi
	*/
	public void saveDB(String filename){
		PrintWriter file;
		try {
			file = new PrintWriter(filename);
		} catch (FileNotFoundException e) {
			System.out.println("Blad zapisu");
			file=null;
			e.printStackTrace();
		}
		if(file==null) return;
		for(int i=0; i<dict.size(); i++){
			file.println(dict.get(i).getWord());
			file.println(dict.get(i).getClue());
		}
		file.close();
	}
	/**
	 * Metoda zwracajaca dlugosc listy
	 * @return int
	 */
	public int getSize(){
		return dict.size();
	}
	/**
	 * Metoda otwierajaca plik
	 * @param filename String
	 * @throws FileNotFoundException 
	 */
	protected void createDB(String filename) throws FileNotFoundException{
		
		Scanner plik;
		plik=new Scanner(new File(filename));
		LinkedList<Entry> lista=new LinkedList<Entry>();
		while(true){
		lista.add(new Entry(plik.nextLine().toUpperCase(), plik.nextLine())); //wszystko jako duze litery
		if(!plik.hasNext()) break;
		}
		plik.close();
		dict=lista;
	}
	/**
	 * Metoda zwracajaca element z listy
	 * @param i int
	 * @return Entry
	 */
	public Entry getDict(int i){
		return dict.get(i);
	}
	/**
	 * Metoda sprawdzajaca czy dane Entry znajduje sie juz na liscie
	 * @param entry Entry
	 * @return boolean
	 */
	public boolean contains(Entry entry){
		for(int i=0; i<dict.size(); i++){
			if(dict.get(i).equals(entry)) return true;
		}
		return false;
	}
}

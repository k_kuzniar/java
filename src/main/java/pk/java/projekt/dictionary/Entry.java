package pk.java.projekt.dictionary;

import java.io.Serializable;

/**
 * Klasa przechowujaca slowo i podpowiedz
 *
 * @see CwEntry
 */
public class Entry implements Serializable{

	private String word;
	private String clue;
	
	/**
	 * Konstruktor przyjmujacy solwo i pytanie
	 * @param w String
	 * @param c String
	 * @see CwEntry#CwEntry(String, String) CwEntry(String, String)
	 */
	public Entry(String w, String c) {
		word=new String(w);
		clue=new String(c);
	}
	/**
	 * Zwraca slowo
	 * @return String
	 */
	public String getWord(){
		return word;
	}
	/**
	 * Zwraca pytanie
	 * @return String
	 */
	public String getClue(){
		return clue;
	}
	/**
	 * Wypisuje slowo i podpowiedz
	 * Uzywane do testow w trybie tekstowym
	 */
	public void out(){
		System.out.println(word+" "+clue);
	}
}

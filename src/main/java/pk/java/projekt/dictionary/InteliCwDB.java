package pk.java.projekt.dictionary;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

import pk.java.projekt.Exeption.noWordException;

/**
 * Klasa zawierajaca liste slow
 *
 * @see CwDB CwDB
 */
public class InteliCwDB extends CwDB implements Serializable{
	
	/**
	 * Konstruktor przyjmujacy scierzke do pliku
	 * @param filename String
	 * @throws FileNotFoundException 
	 * @see CwDB#CwDB(String filename) CwDB(String filename)
	 */
	public InteliCwDB(String filename) throws FileNotFoundException{
		super(filename);
	}
	/**
	 * Konstruktor bezparametrowy
	 * Potrzebny do generowania nowej krzyzowki
	 * @see CwDB#CwDB() CwDB()
	 */
	public InteliCwDB(){
		super();
	}
	/**
	 * Znajduje wszystkie slowa pasujace do wyrazenia regularnego
	 * @param pattern String
	 * @return LinkedList<Entry>
	 */
	public LinkedList<Entry> findAll(String pattern){
		
		LinkedList<Entry> tmp=new LinkedList<Entry>();
		for(int i=0; i<dict.size(); i++){
			if(dict.get(i).getWord().matches(pattern)) tmp.add(dict.get(i));
		}
		return tmp;
	}
	/**
	 * Metoda zwracajaca losowe slowo
	 * @return Entry
	 */
	public Entry getRandom(){
		
		Random generator = new Random();
		return dict.get(generator.nextInt(dict.size()));
	}
	/**
	 * Metoda zwracajaca losowe slowo o danej dlugosci
	 * @param length int
	 * @return Entry
	 * @throws noWordException 
	 */
	public Entry getRandom(int length) throws noWordException{
		
		Random generator=new Random();
		String pattern=new String("^");;
		for(int i=0; i<length; i++){
			pattern+=".";
		}
		pattern+="$";
		LinkedList<Entry> slowa=findAll(pattern);
		if(slowa.size()==0) throw new noWordException();
		return slowa.get(generator.nextInt(slowa.size()-1));
	}
	/**
	 * Metoda zwracajaca losowe slowo pasujace do wzorca
	 * @param pattern String
	 * @return Entry
	 * @throws noWordException 
	 */
	public Entry getRandom(String pattern) throws noWordException{
	
		Random generator=new Random();
		LinkedList<Entry> tmp=findAll(pattern);
		try{
			return tmp.get(generator.nextInt(tmp.size()));
		}
		catch(Exception e){
			throw new noWordException();
		}
	}
	/**
	 *  Metoda rozszerzajaca dodawanie nowego slowa do listy o wstawianie w kolejnosci alfabetycznej
	 *  @param word String
	 *  @param clue String
	 *  @see CwDB#add(String, String) CwDB.add(String, String)
	 */
	public void add(String word, String clue){
		int i=0;
		while(i<dict.size()){
			if(dict.get(i).getWord().compareToIgnoreCase(word)<0) i++;
			else break;
		}
		dict.add(i, new Entry(word, clue));
	}
	/**
	 * Metoda dodajaca entry
	 * Uzywane do generowania nowej krzyzowki
	 * @param e Entry
	 */
	public void add(Entry e){
		dict.add(e);
	}
	/**
	 * Metoda wypisujaca liste slow
	 * Uzywane do testow w trybie tekstowym
	 */
	public void out(){
		for(int i=0; i<dict.size(); i++){
			System.out.println(dict.get(i).getWord()+"---"+dict.get(i).getClue());
		}
	}
}

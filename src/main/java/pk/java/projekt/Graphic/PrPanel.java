package pk.java.projekt.Graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Iterator;

import javax.swing.JPanel;

import pk.java.projekt.krzyzowka.CwBrowser;

import pk.java.projekt.dictionary.CwEntry;

/**
 * Klasa rysujaca krzyzowke i pytania
 *
 *
 */
public class PrPanel extends JPanel implements Printable {

	private CwBrowser browser=null;
	private static boolean wyp=false;
	
	/**
	 * Konstruktor
	 * @param b CwBrowser
	 */
	public PrPanel(CwBrowser b){
		super();
		browser=b;
		wyp=false;
	}
	/**
	 * Metoda zmieniajaca wypelnienie krzyzowki
	 */
	public void changeWyp(){
		if(wyp) wyp=false;
		else wyp=true;
		
	}
	/**
	 * Metoda ustawiajaca krzyzowke na nie wypelniona
	 */
	public static void NWyp(){
		wyp=false;
	}
	/**
	 * Metoda wywolujaca metode, ktora rysuje krzyzowke
	 */
	@Override
	public int print(Graphics arg0, PageFormat arg1, int arg2) throws PrinterException {
		wyp=false;
		Graphics2D g2d=(Graphics2D) arg0;
		g2d.translate(arg1.getImageableX(), arg1.getImageableY());
		this.paint(g2d);
		return 0;
	}
	/**
	 * Wlasciwa metoda rysujaca krzyzowke
	 */
	@Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.BLACK);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("TimesRoman", Font.PLAIN, 16)); //czcionka i jej rozmiar
        if(browser.getCross()==null) return;
        Iterator<CwEntry> it=browser.getCross().getROEntryIter();
        CwEntry tmp;
        double x = 50; //pozycja
        double y = 20;
        double width = 20;//rozmiar kratki
        double height = 20;
        g2.setColor(Color.LIGHT_GRAY);
        g2.fillRect((int) x, (int) y, 20, (int) 20*(browser.getCross().getHeight()));
        g2.setColor(Color.BLACK);
        for(int j=1; it.hasNext(); j++){
        	tmp=it.next();
        	g2.drawString(String.valueOf(j) + ".", (int) x+7-30, (int) y+15);//numeracja
        	for(int i=0; i<tmp.getWord().length(); i++){
        		g2.draw(new Rectangle2D.Double(x, y, width, height));
        		if(wyp==true) g2.drawString(String.valueOf(tmp.getWord().charAt(i)), (int) x+3, (int) y+16);//odstep miedzy literkami
        		x+=20;
        	}
        	x=50;
        	y+=20;
        }
        x=50;
        if(browser.getCross()==null) return;
        Iterator<CwEntry> iter=browser.getCross().getROEntryIter();
    	for(int i=1; iter.hasNext(); i++){
        	tmp=iter.next();
        	 g2.drawString(i + ". " + tmp.getClue(), (int) x, (int) y+50);
        	 y+=20;
    	}
    }
}

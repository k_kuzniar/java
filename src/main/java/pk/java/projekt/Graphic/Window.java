package pk.java.projekt.Graphic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import pk.java.projekt.krzyzowka.Crossword;
import pk.java.projekt.krzyzowka.CwBrowser;
import pk.java.projekt.krzyzowka.Generator;

import pk.java.projekt.Exeption.NoSuchCross;
import pk.java.projekt.Exeption.mainFileNotFound;
import pk.java.projekt.Exeption.noWordException;
 
/**
 * Klasa tworzaca cale okno
 *
 * @see PrPanel PrPanel
 * @see Tabela Tabela
 */
public class Window extends JFrame {
 
	private CwBrowser browser=new CwBrowser("cross");
	private static PrPanel panel=null;
	Tabela ex=null;
	
	/**
	 * Konstruktor, tworzy caly panel
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws UnsupportedLookAndFeelException
	 */
    public Window() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        super("Krzyzowka");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(50,50);
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        setMinimumSize(new Dimension(500, 300));
        
        setLayout(new BorderLayout());
        
        add(Bar(), BorderLayout.NORTH);
        panel=new PrPanel(browser);
        
        /*JScrollPane spane=new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spane.createVerticalScrollBar();
        spane.setMinimumSize(new Dimension(500, 300));
        add(spane, BorderLayout.CENTER);*/
        
        add(panel, BorderLayout.CENTER);
        
        pack();
        setSize(new Dimension(800, 600));
        setVisible(true);
		
    }
    /**
     * Metoda tworzaca gorny pasek
     * @return JToolBar
     * @see #Bar()
     */
    private JToolBar Bar(){
    	JToolBar bar1 = new JToolBar(JToolBar.HORIZONTAL);
        bar1.setFloatable(false);
        bar1.setBackground(Color.gray);
        JToolBar bar2 = new JToolBar(JToolBar.HORIZONTAL);
        bar2.setFloatable(false);
        bar2.setBackground(Color.gray);
        JToolBar bar3 = new JToolBar(JToolBar.HORIZONTAL);
        bar3.setFloatable(false);
        bar3.setBackground(Color.gray);
        
        JToolBar barmaster = new JToolBar(JToolBar.HORIZONTAL);
        barmaster.setFloatable(false);
        barmaster.setBackground(Color.gray);
        
        final JSpinner height=new JSpinner(new SpinnerNumberModel(10, 4, 12, 1));
        final JSpinner width=new JSpinner(new SpinnerNumberModel(10, 4, 12, 1));
        final JButton gen=new JButton("Generuj nowa");
        final JTextField sciezka=new  JTextField("cross", 1);
        final JButton filemanager=new JButton("...");
        final JButton prev=new JButton(String.valueOf('\u25B2'));
        final JButton next=new JButton(String.valueOf('\u25BC'));
        final JButton lista=new JButton("Lista");
        final JButton wczytaj=new JButton("Wczytaj");
        final JButton drukuj=new JButton("Drukuj");
        final JButton zapisz=new JButton("Zapisz");
        final JButton wypelnij=new JButton("Wypelnij");
        
        bar1.add(height);
        bar1.add(width);
        bar1.add(gen);
        bar2.add(sciezka);
        bar2.add(filemanager); 
        bar2.add(prev);
        bar2.add(next);
        bar2.add(lista);
        bar2.add(wczytaj);
        bar3.add(drukuj);
        bar3.add(zapisz);
        bar3.add(wypelnij);
        
        barmaster.add(bar1);
        barmaster.add(bar2);
        barmaster.add(bar3);
        
        gen.addActionListener(new ActionListener() {
			
        	private void reGenerate() throws FileNotFoundException{
        		if(ex!=null) ex.pullThePlug();
        		try {
					browser.generuj((int)height.getValue(), (int)width.getValue());
				} catch (noWordException e) {
					reGenerate();
				} catch (mainFileNotFound e) {
					JOptionPane.showMessageDialog(new JFrame(),
							"Nie znaleziono pliku glownego.\nPodaj recznie jego lokalizacje",
						    "No main file error",
						    JOptionPane.ERROR_MESSAGE);
					JFileChooser file=new JFileChooser();
					int ret = file.showDialog(panel, "Wybierz plik");
					if (ret == JFileChooser.APPROVE_OPTION) {
	                    Generator.setMainFile(file.getSelectedFile().getAbsolutePath());
	                    reGenerate();
					}
				}
        	}
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					PrPanel.NWyp();
					reGenerate();
				} catch (FileNotFoundException e1) {
					System.out.println("Blad");
				}finally{
					draw_cross();
				}
			}
		});
        
        wypelnij.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(ex!=null) ex.pullThePlug();
				if(panel==null) return;
				panel.changeWyp();
				panel.repaint();
				validate();
			}
		});
        
        drukuj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) { 
				if(ex!=null) ex.pullThePlug();
				if(panel==null) return;
				PrinterJob printerJob=PrinterJob.getPrinterJob();
				PageFormat pf=printerJob.defaultPage();
				pf.setOrientation(PageFormat.PORTRAIT);
				Book book=new Book();
				book.append((Printable) panel, pf);
				printerJob.setPageable(book);
				if(printerJob.printDialog()) {
					try { 
						printerJob.print(); 
						} catch(Exception e1) { 
							JOptionPane.showMessageDialog(null, "Blad drukowania!"); 
							} 
					} 
				}
		});
        
        filemanager.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(ex!=null) ex.pullThePlug();
				JFileChooser file=new JFileChooser();
				file.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int ret = file.showDialog(panel, "Wybierz plik");
				if (ret == JFileChooser.APPROVE_OPTION) {
                    sciezka.setText(file.getSelectedFile().getAbsolutePath());
                    browser.changePath(file.getSelectedFile().getAbsolutePath());
                    try {
						browser.getAllCws();
						if(browser.getNumber()!=0){
							browser.setCross(0);
							draw_cross();
						}
					} catch (FileNotFoundException e) {
						
						e.printStackTrace();
					} catch (NoSuchCross e) {
						JOptionPane.showMessageDialog(new JFrame(),
								"Nie znaleziono zadnej krzyzowki",
							    "No Crossword error",
							    JOptionPane.ERROR_MESSAGE);
					} catch (ClassNotFoundException e) {
						
						e.printStackTrace();
					} catch (IOException e) {
						
						e.printStackTrace();
					}
                    
                }
			}
		});
        prev.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Iterator<Crossword> it=browser.getIterator();
				for(int i=0; i<browser.getNumber(); i++){
					Crossword tmp=it.next();
					if(browser.getCross().getID()==tmp.getID() && i>0){
						browser.setCross(i-1);
						draw_cross();
						break;
					}
				}
			}
		});
        next.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Iterator<Crossword> it=browser.getIterator();
				for(int i=0; i<browser.getNumber(); i++){
					Crossword tmp=it.next();
					if(browser.getCross().getID()==tmp.getID() && it.hasNext()){
						browser.setCross(i+1);
						draw_cross();
						break;
					}
				}
				
			}
		});
        lista.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(ex!=null) ex.pullThePlug();
				ex = new Tabela(browser);
                ex.setVisible(true);
			}
		});
        
        wczytaj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(ex!=null) ex.pullThePlug();
				browser.changePath(sciezka.getText());
				try {
					browser.getAllCws();
					browser.setCross(0);
					draw_cross();
				} catch (FileNotFoundException e1) {
					
				} catch (NoSuchCross e1) {
					JOptionPane.showMessageDialog(new JFrame(),
							"Nie znaleziono zadnej krzyzowki",
						    "No Crossword error",
						    JOptionPane.ERROR_MESSAGE);
				}catch (NumberFormatException e3){
					
				} catch (IOException e1) {
					
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					
					e1.printStackTrace();
				}
			}
		});
        zapisz.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(ex!=null) ex.pullThePlug();
				if(browser.getCross()==null) return;
				try {
					browser.changePath(sciezka.getText());
					browser.write(browser.getCross());
				} catch (NoSuchCross e1) {
					
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}
		});
        
        return barmaster;
    }
    /**
     * Metoda odswiezajaca krzyzowke
     */
    public static void draw_cross(){
    	panel.repaint();
    }
}
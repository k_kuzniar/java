package pk.java.projekt.Graphic;

import java.awt.EventQueue;

import javax.swing.UnsupportedLookAndFeelException;

/**
 * Main
 *
 *
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                Window frame;
				try {
					frame = new Window();
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
            }
        }); 
    } 
}
package pk.java.projekt.Graphic;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import pk.java.projekt.krzyzowka.Crossword;
import pk.java.projekt.krzyzowka.CwBrowser;

/**
 * Klasa tworzaca liste krzyzowek
 *
 *
 */
public class Tabela extends JFrame {
    private JList list;
    private CwBrowser browser;

/**
 * Konstruktor
 * @param b CwBrowser
 */
    public Tabela(CwBrowser b) {
    	browser=b;
    	initUI();
    }
/**
 * Metoda inicjalizujaca liste
 */
    private void initUI() {

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        String[] nazwy=new String[browser.getNumber()];
        Iterator<Crossword> it= browser.getIterator();
        for(int i=0; i<browser.getNumber(); i++){
        	nazwy[i]=String.valueOf(it.next().getID());
        }

        list = new JList(nazwy);
        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    String name = (String) list.getSelectedValue();
                    Iterator<Crossword> it= browser.getIterator();
                    int i;
                    for(i=0;it.hasNext(); i++){
                    	Crossword tmp=it.next();
                    	if(name.equals(String.valueOf(tmp.getID()))) break;
                    }
                    PrPanel.NWyp();
                    browser.setCross(i);
                    Window.draw_cross();
                }
            }
        });

        JScrollPane pane = new JScrollPane();
        pane.getViewport().add(list);
        pane.setPreferredSize(new Dimension(250, 200));
        panel.add(pane);

        add(panel);

        pack();
        setTitle("Dostepne krzyzowki");
        setLocation(850,50);
    }
    /**
     * Metoda zamykajaca okno
     */
    public void pullThePlug() {
        WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
}
}
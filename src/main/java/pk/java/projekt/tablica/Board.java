package pk.java.projekt.tablica;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Klasa reprezentujac tablice krzyzowwki
 *
 * @see BoardCell BoardCell
 */
public class Board implements Serializable{//warto by zaimplementowac interfejs clonable

	private int width, height;
    private BoardCell[][] board;
    
    /**
     * Zwraca szerokosc (width)
     * @return int
     */
    public int getWidth(){
    	return width;
    }
    /**
     * Zwraca wysokosc (height)
     * @return int
     */
    public int getHeight(){
    	return height;
    }
    /**
     * Metoda zwracajaca zawartosc danej komorki
     * @param x int wspolrzedna
     * @param y int wspolrzedna
     * @return BoardCell
     * @see BoardCell BoardCell
     */
    public BoardCell getCell(int x, int y){
    	return board[x][y];
    }
    /**
     * Metoda ustawiajaca wartosc danej komorki
     * @param x int wspolrzedna
     * @param y int wspolrzedna
     * @param c BoardCell wartosc do zmiany
     * @see BoardCell BoardCell
     */
    public void setCell(int x, int y, BoardCell c){
    	if(x>=0 && x<height && y>=0 && y<width) board[x][y]=c;
    }
    /**
     * Metoda zwracajaca wszystkie komorki, ktore moga byc poczatkiem dla nowego hasla
     * @return LinkedList<BoardCell>
     */
    public LinkedList<BoardCell> getStartCells(){
    	LinkedList<BoardCell> lista=new LinkedList<BoardCell>();
    	for(int i=0; i<width; i++){
    		for(int j=0; j<height; j++){
    			if(board[i][j].getPropeties().able==true) lista.add(board[i][j]);
    		}
    	}
    	return lista;
    }
    /**
     * Metoda tworzaca wzorzec wyrazenia regularnego
     * Uwaga dziala tylko w wersji prostej
     * @param fromx int
     * @param fromy int
     * @param tox int
     * @param toy int
     * @return String
     */
    public String createPattern(int fromx, int fromy, int tox, int toy){
    	String regexp=new String("^");
    	regexp+=board[fromx][fromy].getContent();
    	regexp+=".{1," + (toy-1) + "}";
    	return regexp;	
    }
    /**
     * Konstruktor
     * @param x int
     * @param y int
     */
    public Board(int x, int y){
    	height=x;
    	width=y;
    	board=new BoardCell[height][];
    	for(int i=0; i<height; i++){
    		board[i]=new BoardCell[width];
    	}
    }
    /**
     * Konstruktor kopiujacy
     * Uzywany przez funkcje getBoardCopy z Crossword
     * @param w Board
     */
    public Board(Board w){
    	width=w.width;
    	height=w.height;
    	board=new BoardCell[height][];
		for(int i=0; i<height; i++){
			board[i]=new BoardCell[width];
		}
		for(int i=0; i<height; i++){
			for(int j=0; j<width; j++){
				if(w.board[i][j]==null) board[i][j]=null;
				else board[i][j]=new BoardCell(w.board[i][j]);
			}
		}
    }

}

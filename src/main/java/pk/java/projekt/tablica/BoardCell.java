package pk.java.projekt.tablica;

import java.io.Serializable;

/**
 * Klasa reprezentujaca pojedyncza komorke w krzyzowce
 *
 * @see Board Board
 */
public class BoardCell implements Serializable{

	private String con;
	/**
	 * Klasa przechowujaca dane odnosnie czy komorka moze byc poczatkiem/koncem/srodkiem dla nowego hasla poziomo, lub pionowo
	 *
	 *
	 */
	public static class Dane implements Serializable{
		public boolean able;
		public boolean vert;
		public enum Poz{
			Start, End, Inner
		}
		public Poz poz;
	}
	private Dane propeties;
	
	/**
	 * Metoda umozliwiajaca ustalenie czy komorka moze byc poczatkiem/koncem/srodkiem dla nowego hasla poziomo, lub pionowo
	 * @return Dane
	 */
	public Dane getPropeties(){
		return propeties;
	}
	/**
	 * Metoda do ustawiania zawartosci danej komorki
	 * @param content String
	 */
	public void setConntent(String content){
		con=content;
	}
	/**
	 * Metoda do pobierania zawartosci danej komorki
	 * @return String
	 */
	public String getContent(){
		return con;
	}
	/**
	 * Konstruktor kopiujacy
	 * Uzywany przez funkcje getBoardCopy z Crossword
	 * @param w BoardCell
	 */
	public BoardCell(BoardCell w){
		this.con=new String(w.con);
		propeties=new Dane();
		this.propeties.able=propeties.able;
		this.propeties.poz=propeties.poz;
		this.propeties.vert=propeties.vert;
		}
	/**
	 * Konstruktor ustawiajacy wszystkie wartosci
	 * @param con String
	 * @param able boolean
	 * @param vert boolean
	 * @param poz dictionary.BoardCell.Dane.Poz
	 */
	public BoardCell(String con, boolean able, boolean vert, BoardCell.Dane.Poz poz){
		this.con=con;
		propeties=new Dane();
		this.propeties.able=able;
		this.propeties.vert=vert;
		this.propeties.poz=poz;
	}
}

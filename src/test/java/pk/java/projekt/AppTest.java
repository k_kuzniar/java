package pk.java.projekt;

import pk.java.projekt.dictionary.Entry;
import pk.java.projekt.krzyzowka.Crossword;
import pk.java.projekt.tablica.Board;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void test1()
    {
    	Entry wpis = new Entry("raz", "dwa");
        assertTrue(wpis.getWord().equals("raz"));
    }
    
    public void test2()
    {
    	Entry wpis = new Entry("raz", "dwa");
        assertTrue(wpis.getClue().equals("dwa"));
    }
    
    public void test3()
    {
    	Board wpis = new Board(5, 10);
        assertTrue(wpis.getWidth() == 10);
    }
    
    public void test4()
    {
    	Board wpis = new Board(5, 10);
        assertTrue(wpis.getHeight() == 5);
    }
    
    public void test5()
    {
    	Crossword wpis = new Crossword(1, 5, 10);
        assertTrue(wpis.getID() == 1);
    }
}
